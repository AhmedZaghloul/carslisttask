//
//  AuctionInfo.swift
//  CarsList
//
//  Created by Ahmed Zaghloul on 7/9/18.
//  Copyright © 2018 AhmedZaghloul. All rights reserved.
//

import Foundation
/**
 Car Model.
 ````
 public var bids : Int?
 public var endDate : Int?
 public var endDateEn : String?
 public var endDateAr : String?
 public var currencyEn : String?
 public var currencyAr : String?
 public var currentPrice : Double?
 public var minIncrement : Int?
 public var lot : Int?
 public var priority : Int?
 public var vATPercent : Int?
 public var isModified : Int?
 public var itemid : Int?
 public var iCarId : Int?
 public var iVinNumber : String?
 ````
 
 - bids : No. of bids on the object (Car)
 - endDate : End date of the Auction (of type Int) using time stamp
 - endDateEn : End date in English of the Auction (of type String) using time stamp
 - endDateAr : End date in Arabic of the Auction (of type String) using time stamp
 - currencyEn : Currency of the object in english
 - currencyAr : Currency of the object in Arabic
 - currentPrice : Current price of the object
 - minIncrement : ...
 - lot : ...
 - priority : ...
 - vATPercent : ...
 - isModified : ...
 - itemid : ...
 - iCarId : ...
 - iVinNumber : ...
 
 ## Important Notes ##
 - endDate Attribute is a time stamp of type Int?
 */
public class AuctionInfo {
    public var bids : Int?
    public var endDate : Int?
    public var endDateEn : String?
    public var endDateAr : String?
    public var currencyEn : String?
    public var currencyAr : String?
    public var currentPrice : Double?
    public var minIncrement : Int?
    public var lot : Int?
    public var priority : Int?
    public var vATPercent : Int?
    public var isModified : Int?
    public var itemid : Int?
    public var iCarId : Int?
    public var iVinNumber : String?
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let auctionInfo = AuctionInfo(someDictionaryFromJSON)
     
     - parameter data: AnyObject from JSON.
     
     - returns: AuctionInfo Instance.
     */
    
    init(data:AnyObject){
        if let data = data as? NSDictionary {
            bids = data.getValueForKey(key:"bids", callback: 0)
            endDate = data.getValueForKey(key:"endDate", callback: 0)
            endDateEn = data.getValueForKey(key:"endDateEn", callback: "")
            endDateAr = data.getValueForKey(key:"endDateAr", callback: "")
            currencyEn = data.getValueForKey(key:"currencyEn", callback: "")
            currencyAr = data.getValueForKey(key:"currencyAr", callback: "")
            currentPrice = data.getValueForKey(key:"currentPrice", callback: 0)
            minIncrement = data.getValueForKey(key:"minIncrement", callback: 0)
            lot = data.getValueForKey(key:"lot", callback: 0)
            priority = data.getValueForKey(key:"priority", callback: 0)
            vATPercent = data.getValueForKey(key:"VATPercent", callback: 0)
            isModified = data.getValueForKey(key:"isModified", callback: 0)
            itemid = data.getValueForKey(key:"itemid", callback: 0)
            iCarId = data.getValueForKey(key:"iCarId", callback: 0)
            iVinNumber = data.getValueForKey(key:"iVinNumber", callback: "")
        }
    }
    
}
