//
//  Car.swift
//  CarsList
//
//  Created by Ahmed Zaghloul on 7/9/18.
//  Copyright © 2018 AhmedZaghloul. All rights reserved.
//

import Foundation
/**
 Car Model.
 ````
 public var carID : Int?
 public var image : String?
 public var descriptionAr : String?
 public var descriptionEn : String?
 public var imgCount : Int?
 public var sharingLink : String?
 public var sharingMsgEn : String?
 public var sharingMsgAr : String?
 public var mileage : String?
 public var makeID : Int?
 public var modelID : Int?
 public var bodyId : Int?
 public var year : Int?
 public var makeEn : String?
 public var makeAr : String?
 public var modelEn : String?
 public var modelAr : String?
 public var bodyEn : String?
 public var bodyAr : String?
 public var auctionInfo : AuctionInfo?
 ````
 
 - carID : ID of the Object.
 - image : Image Url
 - descriptionAr : Containing The Description in arabic
 - descriptionEn : ...
 - imgCount : ...
 - sharingLink : ...
 - sharingMsgEn : ...
 - sharingMsgAr : ...
 - mileage : ...
 - makeID : ...
 - modelID : ...
 - bodyId : ...
 - year : ...
 - makeEn : ...
 - makeAr : ...
 - modelEn : ...
 - modelAr : ...
 - bodyEn : ...
 - bodyAr : ...
 - auctionInfo : Auction Model of type **AuctionInfo** Class
 
 ## Important Notes ##
 - auctionInfo Attribute is of type **AuctionInfo**
 */
public class Car {
    public var carID : Int?
    public var image : String?
    public var descriptionAr : String?
    public var descriptionEn : String?
    public var imgCount : Int?
    public var sharingLink : String?
    public var sharingMsgEn : String?
    public var sharingMsgAr : String?
    public var mileage : String?
    public var makeID : Int?
    public var modelID : Int?
    public var bodyId : Int?
    public var year : Int?
    public var makeEn : String?
    public var makeAr : String?
    public var modelEn : String?
    public var modelAr : String?
    public var bodyEn : String?
    public var bodyAr : String?
    public var auctionInfo : AuctionInfo?
    
    /**
     Constructs the object based on the given dictionary.
     
     Sample usage:
     let car = Car(someDictionaryFromJSON)
     
     - parameter dictionary:  AnyObject from JSON.
     
     - returns: Car Instance.
     */
    
    init(data:AnyObject){
        if let data = data as? NSDictionary {
            carID = data.getValueForKey(key:"carID", callback: 0)
            image = data.getValueForKey(key:"image", callback: "")
            descriptionAr = data.getValueForKey(key:"descriptionAr", callback: "")
            descriptionEn = data.getValueForKey(key:"descriptionEn", callback: "")
            imgCount = data.getValueForKey(key:"imgCount", callback: 0)
            sharingLink = data.getValueForKey(key:"sharingLink", callback: "")
            sharingMsgEn = data.getValueForKey(key:"sharingMsgEn", callback: "")
            sharingMsgAr = data.getValueForKey(key:"sharingMsgAr", callback: "")
            mileage = data.getValueForKey(key:"mileage", callback: "")
            makeID = data.getValueForKey(key:"makeID", callback: 0)
            modelID = data.getValueForKey(key:"modelID", callback: 0)
            bodyId = data.getValueForKey(key:"bodyId", callback: 0)
            year = data.getValueForKey(key:"year", callback: 0)
            makeEn = data.getValueForKey(key:"makeEn", callback: "")
            makeAr = data.getValueForKey(key:"makeAr", callback: "")
            modelEn = data.getValueForKey(key:"modelEn", callback: "")
            modelAr = data.getValueForKey(key:"modelAr", callback: "")
            bodyEn = data.getValueForKey(key:"bodyEn", callback: "")
            bodyAr = data.getValueForKey(key:"bodyAr", callback: "")
            auctionInfo = AuctionInfo(data: data.getValueForKey(key: "AuctionInfo", callback: [:]) as AnyObject)
        }
    }
}
