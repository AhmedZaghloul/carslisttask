//
//  CarsListVC.swift
//  CarsList
//
//  Created by Ahmed Zaghloul on 7/8/18.
//  Copyright © 2018 AhmedZaghloul. All rights reserved.
//

import UIKit

class CarsListVC: UIViewController ,SortingDelegate{

    @IBOutlet weak var headerView:UIView!
    @IBOutlet weak var optionsView:UIView!
    @IBOutlet weak var tableView:UITableView!
    
    var refreshControl: UIRefreshControl!
    var bottomSheetVC :FilterVC? = nil

    var cars = [CarViewModel]()
    var sortedCars = [CarViewModel]()
    var timer:Timer?
    var selectedSortIndex = 3
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addRefreshControl()
        self.getData()
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        gradientHeaderView()
    }

    fileprivate func addRefreshControl(){
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Scroll down to Refresh")
        refreshControl.addTarget(self, action: #selector(getData), for: UIControlEvents.valueChanged)
        tableView?.addSubview(refreshControl)
    }
    
    fileprivate func gradientHeaderView() {
        var colors = [CGColor]()
        colors.append(UIColor(red: 255/255, green: 45/255, blue: 74/255, alpha: 1).cgColor)
        colors.append(UIColor(red: 255/255, green: 98/255, blue: 62/255, alpha: 1).cgColor)
        
        let gradient = CAGradientLayer()
        gradient.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.headerView.frame.height)
        gradient.colors = colors
        gradient.startPoint = CGPoint(x: 0, y: 0)
        gradient.endPoint = CGPoint(x: 1, y: 0)
        headerView.layer.insertSublayer(gradient, at: 0)
        
        optionsView.dropShadow()
    }
    
    @objc fileprivate func getData() {
        if self.refreshControl.state == .normal {
            timer?.invalidate()
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
            self.refreshControl.beginRefreshing()
            RequestManager.defaultManager.getCars { (error, cars, refreshInterval) in
                if !error {
                    self.sortedCars.removeAll()
                    self.selectedSortIndex = 3
                    self.cars = cars?.map({return CarViewModel(car: $0)}) ?? []
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                    self.timer?.fire()
                }else{
                    
                }
                DispatchQueue.main.async {
                    self.refreshControl.endRefreshing()
                }
            }
        }
    }
    
    @objc func updateTimer() {
        cars = cars.map { (car: CarViewModel) -> CarViewModel in
            var modifiedCar = car
            if modifiedCar.secondsToEnd > 0 {
                modifiedCar.secondsToEnd -= 1
            }
            return modifiedCar
        }
        if sortedCars.count != 0 {
            sortedCars = sortedCars.map { (car: CarViewModel) -> CarViewModel in
                var modifiedCar = car
                if modifiedCar.secondsToEnd > 0 {
                    modifiedCar.secondsToEnd -= 1
                }
                return modifiedCar
            }
        }
    }
    
}

extension CarsListVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sortedCars.count == 0 ? cars.count : sortedCars.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:
            "CarCell", for: indexPath) as! CarTableViewCell
        
        let car = sortedCars.count == 0 ? cars[indexPath.row] : sortedCars[indexPath.row]
        cell.car = car

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.height / 4.9
    }
    
    @IBAction func addBottomSheetViewForSorting() {
        if bottomSheetVC == nil{
            bottomSheetVC = FilterVC()
            bottomSheetVC?.view.dropShadow()
            bottomSheetVC?.selectedSortIndex = self.selectedSortIndex
            self.addChildViewController(bottomSheetVC!)
            self.view.addSubview(bottomSheetVC!.view)
            bottomSheetVC!.didMove(toParentViewController: self)
            bottomSheetVC!.delegate = self
            let height = view.frame.height * 0.89
            let width  = view.frame.width
            bottomSheetVC!.view.frame = CGRect(x: 0, y: self.view.frame.maxY, width: width, height: height)
        }
    }

    func didSelectItemToSortWith(ItemIndex index: Int) {
        self.selectedSortIndex = index
        switch index {
        case 0://price
            sortedCars = cars.sorted(by: { $0.price < $1.price })
        case 1://year
            sortedCars = cars.sorted(by: { $0.year > $1.year })
        case 2://End Date
            sortedCars = cars.sorted(by: { $0.endDate > $1.endDate })
        default:
            sortedCars.removeAll()
        }
        
        DispatchQueue.main.async {
            self.tableView.beginUpdates()
            UIView.transition(with: self.tableView,
                              duration: 0.35,
                              options: .transitionCrossDissolve,
                              animations: { self.tableView.reloadData() })
            self.tableView.endUpdates()
        }
    }
    
    func sortViewDidDismissed() {
        self.bottomSheetVC = nil
    }
}
