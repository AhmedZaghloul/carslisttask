//
//  CarTableViewCell.swift
//  CarsList
//
//  Created by Ahmed Zaghloul on 7/9/18.
//  Copyright © 2018 AhmedZaghloul. All rights reserved.
//

import UIKit
import Kingfisher

class CarTableViewCell: UITableViewCell {

    @IBOutlet weak var holderView:UIView!
    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var priceLabel:UILabel!
    @IBOutlet weak var currencyLabel:UILabel!

    @IBOutlet weak var lotNumberLabel:UILabel!
    @IBOutlet weak var bidsLabel:UILabel!
    @IBOutlet weak var timeLeftLabel:UILabel!

    @IBOutlet weak var carImageView:UIImageView!
    @IBOutlet weak var likeButton:LikeButton!

    var car:CarViewModel! {
        didSet{
                DispatchQueue.main.async {
                    self.titleLabel.text = self.car.title
                    
                    self.priceLabel.text = self.car.priceStr
                    
                    self.lotNumberLabel.text = "\(self.car.lots)"
                    self.bidsLabel.text = "\(self.car.bids)"
                    self.timeLeftLabel.text = self.car.intervalStr
                    self.currencyLabel.text = self.car.currency
                    self.timeLeftLabel.textColor = ((self.car.secondsToEnd / 3600) * 60) > 5 ? appDark : appRed
                    
                    var modifiedUrl = (self.car.imgUrlStr.replacingOccurrences(of: "[w]", with: "\(self.frame.width)"))
                    modifiedUrl = modifiedUrl.replacingOccurrences(of: "[h]", with: "\(self.frame.height)")
                    let url = URL(string: modifiedUrl)
                    
                    self.carImageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeholder"), options: [.transition(ImageTransition.fade(1))], progressBlock: { receivedSize, totalSize in
                    }, completionHandler: nil)
                }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        holderView.dropShadow()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
  
}
