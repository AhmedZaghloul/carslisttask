//
//  LikeButton.swift
//  CarsList
//
//  Created by Ahmed Zaghloul on 7/9/18.
//  Copyright © 2018 AhmedZaghloul. All rights reserved.
//

import UIKit

class LikeButton: UIButton {
    
    //images
    var checkedImage = #imageLiteral(resourceName: "filled-heart")
    var unCheckedImage = #imageLiteral(resourceName: "empty-heart")
    
    //bool propety
    var isChecked:Bool = false{
        didSet{
            
            if isChecked == true{
                self.setImage(checkedImage, for: .normal)
            }else{
                self.setImage(unCheckedImage, for: .normal)
            }
            
        }
    }
    
    override func awakeFromNib() {
        isChecked = false
    }
    
}
