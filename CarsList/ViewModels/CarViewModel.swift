//
//  CarViewModel.swift
//  CarsList
//
//  Created by Ahmed Zaghloul on 7/9/18.
//  Copyright © 2018 AhmedZaghloul. All rights reserved.
//

import Foundation

struct CarViewModel{
    let title :String
    let price :Double
    let priceStr :String
    let currency :String
    let bids :Int
    let lots :Int
    let year :Int
    let endDate :Int
    var intervalStr : String
    let imgUrlStr:String
    
    
    var secondsToEnd :Int{
        didSet{
            intervalStr = "\((self.secondsToEnd / 3600)):\((self.secondsToEnd % 3600) / 60):\((self.secondsToEnd % 3600) % 60)"
        }
    }
    
    init(car:Car) {
        self.title = "\(car.makeEn ?? "") \(car.modelEn ?? "") \(car.year ?? 2000)"
        
        self.priceStr = "\(car.auctionInfo?.currentPrice ?? 0)"
        self.price = car.auctionInfo?.currentPrice ?? 0.0
        self.currency = car.auctionInfo?.currencyEn ?? ""
        
        self.lots = car.auctionInfo?.lot ?? 0
        self.bids = car.auctionInfo?.bids ?? 0
        self.secondsToEnd = car.auctionInfo?.endDate ?? 0
        self.intervalStr = "\((self.secondsToEnd / 3600)):\((self.secondsToEnd % 3600) / 60):\((self.secondsToEnd % 3600) % 60)"
        self.imgUrlStr = car.image ?? ""
        self.year = car.year ?? 0
        self.endDate = car.auctionInfo?.endDate ?? 0
    }
}
