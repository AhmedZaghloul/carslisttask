//
//  Extensions.swift
//  CarsList
//
//  Created by Ahmed Zaghloul on 7/9/18.
//  Copyright © 2018 AhmedZaghloul. All rights reserved.
//

import Foundation
import UIKit

let SERVICE_URL_PREFIX = "http://api.emiratesauction.com/v2/"

extension UIView{
    func dropShadow(scale: Bool = true) {
        DispatchQueue.main.async {
            self.layer.masksToBounds = false
            self.layer.shadowColor = UIColor.black.cgColor
            self.layer.shadowOpacity = 0.3
            self.layer.shadowOffset = CGSize(width: 0, height: 1)
            self.layer.shadowRadius = 1.5
            self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
            self.layer.shouldRasterize = true
            self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
        }
    }
}
extension UITableView {
    open override func awakeFromNib() {
        super.awakeFromNib()
        self.tableFooterView = UIView()
    }
}

enum VendingMachineError:Error {
    case valueNotFounds
}

extension NSDictionary {
    func getValueForKey<T>(key:String,callback:T)  -> T{
        guard let value  = self[key] as? T else{
            return callback}
        return value
    }
    func getValueForKey<T>(key:String) throws -> T{
        guard let value  = self[key] as? T else{throw VendingMachineError.valueNotFounds}
        return value
    }
}

extension NSMutableURLRequest{
    func setBodyConfigrationWithMethod(method:String){
        self.httpMethod = method
    }
}

let appGreen = UIColor(red: 135/255, green: 209/255, blue: 85/255, alpha: 1.0)
let appRed = UIColor(red: 255/255, green: 68/255, blue: 68/255, alpha: 1.0)
let appDark = UIColor(red: 63/255, green: 63/255, blue: 63/255, alpha: 1.0)
let appGray = UIColor(red: 231/255, green: 234/255, blue: 236/255, alpha: 1.0)

protocol SortingDelegate :class {
    func didSelectItemToSortWith(ItemIndex index:Int)
    func sortViewDidDismissed()
}
