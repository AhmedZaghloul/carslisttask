//
//  RequestsManager.swift
//  CarsList
//
//  Created by Ahmed Zaghloul on 7/9/18.
//  Copyright © 2018 AhmedZaghloul. All rights reserved.
//

import Foundation
/**
 **Manager of all API Reqesuts**.
 ````
 static let defaultManager = RequestManager()
 private let requestTimourInterval = 20.0
 ````
 
 - defaultManager: Default manager to confirm singleton pattern.
 - requestTimourInterval: Maximum time taken for the request.
 
 ## Important Notes ##
 - This Class Confirms **Singleton Design Pattern**
 
 */
class RequestManager{
    static let defaultManager = RequestManager()
    private init (){}
    private let requestTimoutInterval = 20.0

    /**
     Requesting list of cars from the API.
     Returns Closure With Parameters :
     - Parameter error: Boolean if the request has been done successfully.
     - Parameter car: Array of the returned objects.
     - Parameter refreshInterval: To know when to refresh the data.
     
     ## Important Notes ##
     - The Service is a **GET** method.
     */
    func getCars(compilition : @escaping (_ error : Bool,_ cars:[Car]?,_ refreshInterval:Int?)->Void){
        let mutableURLRequest = NSMutableURLRequest(url: URL(string: SERVICE_URL_PREFIX + "carsonline")!,
                                                    cachePolicy: .useProtocolCachePolicy,
                                                    timeoutInterval: requestTimoutInterval)
        mutableURLRequest.setBodyConfigrationWithMethod(method: "GET")
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: mutableURLRequest as URLRequest, completionHandler: { (data, response, error) -> Void in
            if let res:HTTPURLResponse = response as? HTTPURLResponse {
                print(res.statusCode)
                if (error != nil || res.statusCode != 200) {
                    compilition(true,nil,nil)
                    return
                } else {
                    var json: NSDictionary!
                    do {
                        json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as! NSDictionary

                        var refreshInterval:Int!
                        var cars:[Car] = []
                        
                        if let response = json["Cars"] as? [NSDictionary] {
                            refreshInterval = json.getValueForKey(key: "RefreshInterval", callback: 10)
                            
                            for object in response {
                                cars.append(Car(data: object as AnyObject))
                            }
                            
                            compilition(false,cars,refreshInterval)
                            return
                        }else{
                            compilition(true,nil,nil)
                        }
                        
                    } catch {
                        compilition(true,nil,nil)
                    }
                }
            }else {
                compilition(true,nil,nil)
            }
            
        })
        dataTask.resume()
    }
}
